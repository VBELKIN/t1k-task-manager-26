package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "view-user-profile";

    @NotNull
    private static final String DESCRIPTION = "view profile of current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }


}
