package ru.t1k.vbelkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.api.repository.IRepository;
import ru.t1k.vbelkin.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(@Nullable String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }


    @NotNull
    @Override
    public M findOneByIndex(Integer index) {
        return models.get(index);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return models.size();
    }

    @NotNull
    @Override
    public M remove(@NotNull M model) {
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull Integer index) {
        @NotNull final M model = findOneByIndex(index);
        if (model == null) return null;
        models.remove(model);
        return model;
    }

}
